package com.botpoc.botpoc.repository;

import com.botpoc.botpoc.entity.BotInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BotInfoRepository extends JpaRepository<BotInfo, Integer> {

//    SELECT b.*, a.name FROM tableB AS b INNER JOIN tableA as A ON (b.id=a.id);
//    @Query("SELECT * FROM bot_schedule WHERE si_fk =?1")
//    public List<BotInfo> getschedule(int id);
}
