package com.botpoc.botpoc.repository;

import com.botpoc.botpoc.entity.BotContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BotContentRepository extends JpaRepository<BotContent, Integer> {
}
