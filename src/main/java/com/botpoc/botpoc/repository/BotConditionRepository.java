package com.botpoc.botpoc.repository;

import com.botpoc.botpoc.entity.BotCondition;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BotConditionRepository extends JpaRepository<BotCondition, Integer> {
}
