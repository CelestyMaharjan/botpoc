package com.botpoc.botpoc.repository;

import com.botpoc.botpoc.entity.BotReceiver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BotReceiverRepository extends JpaRepository<BotReceiver, Integer> {
}
