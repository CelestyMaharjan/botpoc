package com.botpoc.botpoc.repository;

import com.botpoc.botpoc.entity.BotSchedule;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BotScheduleRepository extends JpaRepository<BotSchedule, Integer>  {

}
