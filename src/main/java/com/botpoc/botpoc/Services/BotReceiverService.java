package com.botpoc.botpoc.Services;

import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotReceiver;

import java.util.List;

public interface BotReceiverService {
    public BotReceiver saveReceiver(InputRequest<BotReceiver> br);
    public BotReceiver findReceiver(int id);
    public List<BotReceiver> findAll();
    public String deleteReceiver(int id );
    public BotReceiver updateReceiver(InputRequest<BotReceiver> req, int id);
}
