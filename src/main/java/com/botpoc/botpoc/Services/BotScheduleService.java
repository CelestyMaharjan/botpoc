package com.botpoc.botpoc.Services;

import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotSchedule;

import java.util.List;

public interface BotScheduleService {
    public BotSchedule saveSchedule(InputRequest<BotSchedule> req);
    public BotSchedule findSchedule(int id);
    public List<BotSchedule> findAll();
    public String deleteSchedule(int id );
    public BotSchedule updateSchedule(InputRequest<BotSchedule> req, int id);

}
