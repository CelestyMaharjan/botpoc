package com.botpoc.botpoc.Services;

import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotInfo;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BotInfoService {
    public BotInfo saveInfo(InputRequest<BotInfo> req);
    public List<BotInfo> findAllInfo();
    public BotInfo finInfoById(int id);
    public BotInfo updateInfo(int id, InputRequest<BotInfo> req);
    public String deleteInfo(int id);

}
