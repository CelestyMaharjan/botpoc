package com.botpoc.botpoc.Services;

import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotContent;

import java.util.List;

public interface BotContentService {
    public BotContent saveContent(InputRequest<BotContent> req);
    public List<BotContent> findAllContent();
    public BotContent findContentById(int id);
    public BotContent updateContent(int id, InputRequest<BotContent> req);
    public String deleteContent(int id);
}
