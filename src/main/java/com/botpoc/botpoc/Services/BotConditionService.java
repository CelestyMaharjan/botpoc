package com.botpoc.botpoc.Services;

import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotCondition;

import java.util.List;

public interface BotConditionService {
    public BotCondition saveCondition(InputRequest<BotCondition> req);
    public BotCondition findConditionById(int id);
    public List<BotCondition> findAllCondition();
    public String deleteCondition(int id);
    public BotCondition updateCondition(InputRequest<BotCondition> req, int id);
}
