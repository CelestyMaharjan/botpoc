package com.botpoc.botpoc.controller;

import com.botpoc.botpoc.Services.BotReceiverService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/botreceiver")
public class BotReceiverController {

    @Autowired
    private BotReceiverService service;

    //post
    @PostMapping("/addReceiver")
    public BotReceiver addReceiver(@RequestBody InputRequest<BotReceiver> br){
        return service.saveReceiver(br);
    }

    //get
    @GetMapping("/getReceiver/{id}")
    public BotReceiver findReceiverById(@PathVariable int id){
        return service.findReceiver(id);
    }

    //get
    @GetMapping("/getAllReceiver")
    public List<BotReceiver> findReceiver(){
        return service.findAll();
    }

    //delete
    @DeleteMapping("/deleteReceiver/{id}")
    public String deleteReceiver(@PathVariable int id){
        return service.deleteReceiver(id);
    }

    //update
    @PutMapping("/updateReceiver/{id}")
    public BotReceiver updateReceiver(@PathVariable int id, @RequestBody InputRequest<BotReceiver> br){
        return service.updateReceiver(br, id);
    }

}
