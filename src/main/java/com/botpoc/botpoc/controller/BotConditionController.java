package com.botpoc.botpoc.controller;

import com.botpoc.botpoc.Services.BotConditionService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotCondition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/botcondition")
public class BotConditionController {
    @Autowired
    private BotConditionService conditionService;

    private static final Logger log = LogManager.getLogger(BotConditionController.class);

    //post
    @PostMapping("/addCondition")
    public BotCondition addCondition(@RequestBody InputRequest<BotCondition> bc){
        log.debug("saving condition from bot condition controller");
        log.info("user is " + bc.getLoggedUser() + "is save");
        return conditionService.saveCondition(bc);
    }

    //get
    @GetMapping("/getAllCondition")
    public List<BotCondition> findAllCondition(){
        return conditionService.findAllCondition();
    }

    @GetMapping("/getConditionById/{id}")
    public BotCondition findById(@PathVariable int id){
        return conditionService.findConditionById(id);
    }

    //update
    @PutMapping("/updateCondition/{id}")
    public BotCondition updateCondition(@RequestBody InputRequest<BotCondition> bc, @PathVariable int id){
        return conditionService.updateCondition(bc, id);
    }

    //delete
    @DeleteMapping("/deleteCondition/{id}")
    public String deleteCondition(@PathVariable int id){
        return conditionService.deleteCondition(id);
    }

}
