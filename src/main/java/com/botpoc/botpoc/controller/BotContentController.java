package com.botpoc.botpoc.controller;

import com.botpoc.botpoc.Services.BotContentService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/botcontent")
public class BotContentController {
    @Autowired
    private BotContentService contentService;

    //post
    @PostMapping("/addContent")
    public BotContent addContent(@RequestBody InputRequest<BotContent> bct){
        return contentService.saveContent(bct);
    }

    //get
    @GetMapping("/getAllContent")
    public List<BotContent> findAllContent(){
        return contentService.findAllContent();
    }

    @GetMapping("/getContentById/{id}")
    public BotContent findContentById(@PathVariable int id){
        return contentService.findContentById(id);
    }

    //updata
    @PutMapping("/updateContent/{id}")
    public BotContent updateContent(@PathVariable int id, @RequestBody InputRequest<BotContent> bct){
        return contentService.updateContent(id, bct);
    }

    //delete
    @DeleteMapping("/deleteContent/{id}")
    public String deleteContent(@PathVariable int id){
        return contentService.deleteContent(id);
    }
}
