package com.botpoc.botpoc.controller;

import com.botpoc.botpoc.Services.BotInfoService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest/botinfo")
public class BotInfoController {
    @Autowired
    private BotInfoService infoService;

    @PostMapping("/addInfo")
    public BotInfo addInfo(@RequestBody InputRequest<BotInfo> bi){
        return infoService.saveInfo(bi);
    }

    @GetMapping("/getInfo")
    public List<BotInfo> findInfo(){
        return  infoService.findAllInfo();
    }

    @GetMapping("/getInfoById/{id}")
    public BotInfo findInfoById(@PathVariable int id){
        return infoService.finInfoById(id);
    }

    @PutMapping("/updateInfo/{id}")
    public BotInfo updateInfo(@PathVariable int id, @RequestBody InputRequest<BotInfo> bi){

        return infoService.updateInfo(id, bi);
    }

    @DeleteMapping("/deleteInfo/{id}")
    public String deleteInfo(@PathVariable int id){
        return infoService.deleteInfo(id);
    }
}
