package com.botpoc.botpoc.controller;

import com.botpoc.botpoc.Services.BotScheduleService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotSchedule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/botschedule")
public class BotScheduleController {

    @Autowired
    private BotScheduleService botScheduleService;

    private static final Logger log = LogManager.getLogger(BotConditionController.class);


    //get
    @GetMapping("/getSchedule/{id}")
    public BotSchedule findScheduleById(@PathVariable int id){
        return botScheduleService.findSchedule(id);
    }

    //get
    @GetMapping("/getAllSchedule")
    public List<BotSchedule> findSchedule(){
        log.debug("hello world");
        return botScheduleService.findAll();
    }

    //delete
    @DeleteMapping("/deleteSchedule/{id}")
    public String deleteSchedule(@PathVariable int id){
        return botScheduleService.deleteSchedule(id);
    }

    //update
    @PutMapping("/updateSchedule/{id}")
    public BotSchedule updateSchedule(@PathVariable int id, @RequestBody InputRequest<BotSchedule> bs){
        return botScheduleService.updateSchedule(bs, id);
    }
}
