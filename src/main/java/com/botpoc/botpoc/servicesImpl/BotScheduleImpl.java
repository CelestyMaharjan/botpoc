package com.botpoc.botpoc.servicesImpl;

import com.botpoc.botpoc.Services.BotScheduleService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotSchedule;
import com.botpoc.botpoc.repository.BotScheduleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BotScheduleImpl implements BotScheduleService {

    @Autowired
    private BotScheduleRepository botScheduleRepo;

    @Override
    public BotSchedule saveSchedule(InputRequest<BotSchedule> req) {
        BotSchedule bs = req.getBot();
        return botScheduleRepo.save(bs);
    }

    @Override
    public BotSchedule findSchedule(int id) {
        return botScheduleRepo.findById(id).orElse(null);
    }

    @Override
    public List<BotSchedule> findAll() {
        return botScheduleRepo.findAll();
    }

    @Override
    public String deleteSchedule(int id) {
       botScheduleRepo.deleteById(id);
        return "id:"+ id + "is deleted";
    }

    @Override
    public BotSchedule updateSchedule(InputRequest<BotSchedule> req, int id) {
        BotSchedule existing = botScheduleRepo.findById(id).orElse(null);
        BotSchedule bs = req.getBot();
        existing.setDay(bs.getDay());
        existing.setMethod(bs.getMethod());
        existing.setTrigger(bs.getTrigger());
        existing.setTriggerType(bs.getTriggerType());
        existing.setWeek(bs.getWeek());

        return botScheduleRepo.saveAndFlush(existing);
    }
}
