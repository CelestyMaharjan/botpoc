package com.botpoc.botpoc.servicesImpl;

import com.botpoc.botpoc.Services.BotConditionService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotCondition;
import com.botpoc.botpoc.repository.BotConditionRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BotConditionServiceImpl implements BotConditionService {

    @Autowired
    private BotConditionRepository conditionRepo;

    private static final Logger log = LogManager.getLogger(BotConditionServiceImpl.class);

    @Override
    public BotCondition saveCondition(InputRequest<BotCondition> req) {
        String currentUser = req.getLoggedUser();
        log.debug("hello my " + currentUser);
        BotCondition bc = req.getBot();
        bc.setCreatedBy(currentUser);
//        System.out.println(bc);
        return conditionRepo.save(bc);
    }

    @Override
    public BotCondition findConditionById(int id) {
        return conditionRepo.findById(id).orElse(null);
    }

    @Override
    public List<BotCondition> findAllCondition() {
        return conditionRepo.findAll();
    }

    @Override
    public String deleteCondition(int id) {
         conditionRepo.deleteById(id);
        return "id "+ id + " has been deleted";
    }

    @Override
    public BotCondition updateCondition(InputRequest<BotCondition> req, int id) {
        String currentUser = req.getLoggedUser();
        BotCondition bc = req.getBot();
        BotCondition existing = conditionRepo.findById(id).orElse(null);
        existing.setCondition(bc.getCondition());
        existing.setTimeframe(bc.getTimeframe());
        existing.setModifiedBy(currentUser);
        return conditionRepo.saveAndFlush(existing);
    }
}
