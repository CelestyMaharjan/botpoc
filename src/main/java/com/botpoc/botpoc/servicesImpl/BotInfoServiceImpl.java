package com.botpoc.botpoc.servicesImpl;

import com.botpoc.botpoc.Services.BotInfoService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotInfo;
import com.botpoc.botpoc.entity.BotSchedule;
import com.botpoc.botpoc.repository.BotInfoRepository;
import com.botpoc.botpoc.repository.BotScheduleRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BotInfoServiceImpl implements BotInfoService {
    @Autowired
    private BotInfoRepository infoRepo;
    @Autowired
    private BotScheduleRepository scheduleRepo;

    private static final Logger log = LogManager.getLogger(BotInfoServiceImpl.class);

    @Override
    public BotInfo saveInfo(InputRequest<BotInfo> req) {
        BotInfo bi = req.getBot();
        String currentUser = req.getLoggedUser();
        log.debug("methods are " + bi.getMethods());
        bi.setCreatedBy(currentUser);
//        scheduleRepo.saveAll(bi.getMethods());

        return infoRepo.save(bi);
    }

    @Override
    public List<BotInfo> findAllInfo() {
        return infoRepo.findAll();
    }

    @Override
    public BotInfo finInfoById(int id) {
        return infoRepo.findById(id).orElse(null);
    }

    @Override
    public BotInfo updateInfo(int id, InputRequest<BotInfo> req) {
        BotInfo bi = req.getBot();
        BotInfo existing = infoRepo.findById(id).orElse(null);
        String currentUser = req.getLoggedUser();
//        bi.setModifiedBy(currentUser);


//        BotSchedule existingMethod = scheduleRepo.getById(id);

        existing.setBotType(bi.getBotType());
        existing.setClientId(bi.getClientId());
        existing.setClientSecret(bi.getClientSecret());

        for(BotSchedule bs: bi.getMethods()){
            BotSchedule existingMethod = scheduleRepo.findById(bs.getId()).orElse(null);
//                log.debug("schedule id :" + bs.getId());
                existingMethod.setDay(bs.getDay());
                existingMethod.setMethod(bs.getMethod());
                existingMethod.setTrigger(bs.getTrigger());
                existingMethod.setTriggerType(bs.getTriggerType());
                existingMethod.setWeek(bs.getWeek());

                scheduleRepo.saveAndFlush(existingMethod);

        }

        existing.setTitle(bi.getTitle());
        existing.setTenantId(bi.getTenantId());
        existing.setModifiedBy(currentUser);

        return infoRepo.saveAndFlush(existing);
    }

    @Override
    public String deleteInfo(int id) {
        infoRepo.deleteById(id);
        return "id "+ id +" is deleted";
    }
}
