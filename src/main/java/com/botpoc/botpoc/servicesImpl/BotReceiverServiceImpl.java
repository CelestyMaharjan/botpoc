package com.botpoc.botpoc.servicesImpl;

import com.botpoc.botpoc.Services.BotReceiverService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotReceiver;
import com.botpoc.botpoc.repository.BotReceiverRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BotReceiverServiceImpl implements BotReceiverService {
    @Autowired
    private BotReceiverRepository receiverRepo;

    private static final Logger log = LogManager.getLogger(BotReceiverServiceImpl.class);

    //save
    @Override
    public BotReceiver saveReceiver(InputRequest<BotReceiver> req){
        String currentUser = req.getLoggedUser();
        log.debug("hello my " + currentUser);
        BotReceiver BotReceiver = req.getBot();
        BotReceiver.setCreatedBy(currentUser);
        return receiverRepo.save(BotReceiver);
    }

    //get
    @Override
    public BotReceiver findReceiver(int id){
        return receiverRepo.findById(id).orElse(null);
    }

    @Override
    public List<BotReceiver> findAll() {
        return receiverRepo.findAll();
    }

    //delete
    @Override
    public String deleteReceiver(int id ){
        receiverRepo.deleteById(id);
        return "id "+id+" is deleted.";
    }

    @Override
    public BotReceiver updateReceiver(InputRequest<BotReceiver> req, int id) {
        BotReceiver br = req.getBot();
        BotReceiver existing = receiverRepo.findById(id).orElse(null);
        existing.setUserId(br.getUserId());
        existing.setModifiedBy(req.getLoggedUser());
        return receiverRepo.saveAndFlush(existing);
    }

    //update
//    @Override
//    public BotReceiver updateReceiver(BotReceiver br, int id){
//        BotReceiver existingReceiver = receiverRepo.findById(id).orElse(null);
//
//
//    }
}
