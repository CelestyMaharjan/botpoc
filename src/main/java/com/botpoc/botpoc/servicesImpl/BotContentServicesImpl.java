package com.botpoc.botpoc.servicesImpl;

import com.botpoc.botpoc.Services.BotContentService;
import com.botpoc.botpoc.dto.InputRequest;
import com.botpoc.botpoc.entity.BotContent;
import com.botpoc.botpoc.repository.BotContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BotContentServicesImpl implements BotContentService {
    @Autowired
    private BotContentRepository contentRepo;

    @Override
    public BotContent saveContent(InputRequest<BotContent> req) {
        String currentUser = req.getLoggedUser();
        BotContent bct = req.getBot();
        bct.setCreatedBy(currentUser);
        return contentRepo.save(bct);
    }

    @Override
    public List<BotContent> findAllContent() {
        return contentRepo.findAll();
    }

    @Override
    public BotContent findContentById(int id) {
        return contentRepo.findById(id).orElse(null);
    }

    @Override
    public BotContent updateContent(int id, InputRequest<BotContent> req) {
        BotContent bct = req.getBot();
        String modified = req.getLoggedUser();
        BotContent existing = contentRepo.findById(id).orElse(null);
        existing.setMessage(bct.getMessage());
        existing.setModifiedBy(modified);
        return contentRepo.saveAndFlush(existing);
    }

    @Override
    public String deleteContent(int id) {
        contentRepo.deleteById(id);
        return "Id "+ id + " has been deleted";
    }
}
