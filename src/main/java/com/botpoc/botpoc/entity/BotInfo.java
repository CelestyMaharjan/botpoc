package com.botpoc.botpoc.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@Table(name = "BotInfo_table")
@SQLDelete(sql = "UPDATE bot_info_table SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
@EntityListeners(AuditingEntityListener.class)
public class BotInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String botType;
    private String clientId;
    private String clientSecret;
    @OneToMany(targetEntity = BotSchedule.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "si_fk", referencedColumnName = "id")
    @SQLDelete(sql = "UPDATE bot_schedule SET si_fk = null WHERE id=?")
//    @Where(clause = "si_fk=")
    private List<BotSchedule> methods;
    private String tenantId;
    private String title;
    @CreatedBy
    private String createdBy;
    @CreatedDate
    private LocalDateTime createdAt;
    @LastModifiedBy
    private String modifiedBy;
    @LastModifiedDate
    private LocalDateTime modifiedAt;
    boolean deleted = false;

}


