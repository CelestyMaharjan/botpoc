package com.botpoc.botpoc.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class InputRequest<ir> {

    @JsonProperty(value = "uId", required = true)
    private String loggedUser;
    private ir bot;
}
